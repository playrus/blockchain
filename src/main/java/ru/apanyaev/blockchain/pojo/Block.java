package ru.apanyaev.blockchain.pojo;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;
import ru.apanyaev.blockchain.util.BlockUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Log4j2
@Accessors(chain = true)
@Table(name = "BLOCK")
public class Block {
    @Id
    private String hash;
    private String previousHash;
    private String data;
    private long timeStamp;

    public Block(String data, String previousHash) {
        this.data = data;
        this.previousHash = previousHash;
        this.timeStamp = new Date().getTime();
        this.hash = BlockUtils.cryptSha256(previousHash + timeStamp + data);
    }

    protected Block() {

    }
}
