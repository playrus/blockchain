package ru.apanyaev.blockchain.service;

import ru.apanyaev.blockchain.pojo.Block;

import java.util.List;

public interface BlockchainService {
    void createBlock(String data);
    boolean validateBlocks();
    List<Block> getBlocks();
}
