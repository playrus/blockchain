package ru.apanyaev.blockchain.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ru.apanyaev.blockchain.dao.BlockchainDao;
import ru.apanyaev.blockchain.pojo.Block;
import ru.apanyaev.blockchain.service.BlockchainService;
import ru.apanyaev.blockchain.util.BlockUtils;

import java.util.List;

@Service
@AllArgsConstructor
public class BlockchainServiceImpl implements BlockchainService {

    private final BlockchainDao blockchainDao;

    @Override
    public void createBlock(String data) {
        List<Block> blocks = blockchainDao.findAll();
        if (CollectionUtils.isEmpty(blocks)) {
            blockchainDao.save(new Block(data, "0"));
            return;
        }
        blockchainDao.save(new Block(data, blocks.get(blocks.size() - 1).getHash()));
    }

    @Override
    public boolean validateBlocks() {
        List<Block> blocks = blockchainDao.findAll();
        if (CollectionUtils.isEmpty(blocks) || blocks.size() == 1) {
            return true;
        }
        return BlockUtils.validateBlock(blocks);
    }

    @Override
    public List<Block> getBlocks() {
        return blockchainDao.findAll();
    }
}
