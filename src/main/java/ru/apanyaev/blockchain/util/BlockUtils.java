package ru.apanyaev.blockchain.util;

import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.CollectionUtils;
import ru.apanyaev.blockchain.pojo.Block;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.List;

@Log4j2
@UtilityClass
public class BlockUtils {

    public String cryptSha256(String input) {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-256");
            int i = 0;
            byte[] hash = sha.digest(input.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexHash = new StringBuilder();
            while (i < hash.length) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexHash.append('0');
                hexHash.append(hex);
                i++;
            }
            return hexHash.toString();
        } catch (Exception ex) {
            log.error("Error: ", ex);
            throw new RuntimeException(ex);
        }
    }

    public boolean validateBlock(List<Block> blocks) {
        Collections.reverse(blocks);
        for (int i = 0; i < blocks.size()-1; i++) {
            if (!blocks.get(i).getPreviousHash().equals(blocks.get(i+1).getHash()))
                return false;
        }
        return true;
    }

}
