package ru.apanyaev.blockchain.manager.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import ru.apanyaev.blockchain.manager.BlockChainManager;
import ru.apanyaev.blockchain.service.BlockchainService;

import java.util.Scanner;

@Component
@AllArgsConstructor
public class BlockChainManagerImpl implements BlockChainManager {

    private final BlockchainService blockchainService;

    private static final String MENU = "Для добавления блока нажмите 1\n" +
            "Для просмотра блокчейна нажмите 2\n" +
            "Для выхода нажмите 3";

    public void process() {
        Scanner in = new Scanner(System.in);
        System.out.println(MENU);
        while (true) {
            switch (in.nextLine()) {
                case "1":
                    addBlock();
                    break;
                case "2":
                    showBlockChain();
                    break;
                case "3":
                    System.exit(0);
                default:
                    System.out.println("Такого пункта меню нет.");
                    return;
            }
        }
    }

    private void addBlock() {
        System.out.println("Введите данные");
        Scanner in = new Scanner(System.in);
        blockchainService.createBlock(in.nextLine());
    }

    private void showBlockChain() {
        System.out.println(blockchainService.getBlocks());
    }

}
