package ru.apanyaev.blockchain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.apanyaev.blockchain.pojo.Block;

import java.util.List;

public interface BlockchainDao extends JpaRepository<Block, String> {
    List<Block> findAll();
}
