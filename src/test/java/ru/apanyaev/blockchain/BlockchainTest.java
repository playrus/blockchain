package ru.apanyaev.blockchain;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import ru.apanyaev.blockchain.pojo.Block;
import ru.apanyaev.blockchain.service.BlockchainService;

import java.util.List;

@SpringBootTest
class BlockchainTest {

    @Autowired
    private BlockchainService blockchainService;

    @Test
    void createBlockTest() {
        blockchainService.createBlock("1");
        blockchainService.createBlock("2");
        blockchainService.createBlock("3");
        List<Block> blocks = blockchainService.getBlocks();
        boolean b = blockchainService.validateBlocks();
        Assert.isTrue(b);
        Assert.isTrue(blocks.size() == 3);
    }

}
